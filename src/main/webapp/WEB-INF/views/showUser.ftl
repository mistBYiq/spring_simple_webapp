<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Show User Page</title>
</head>
<body>
<h1> Show User</h1>

<table>
    <tr>
        <td>User Id</td>
        <td>${user.id}</td>
    </tr>
    <tr>
        <td>User Name</td>
        <td>${user.name}</td>
    </tr>
    <tr>
        <td>User Email</td>
        <td>${user.email}</td>
    </tr>
    <tr>
        <td>User Age</td>
        <td>${user.age}</td>
    </tr>
</table>
<br>
<a href="/users">Back</a>
</body>
</html>